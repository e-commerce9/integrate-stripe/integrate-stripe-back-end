const {Router} = require('express');
const {Stripe} = require('stripe');
const route = Router();
const stripe = Stripe('sk_test_oopVm1zdN2yzByXYao4THoxK00hojwc7Ct');

route.post('/payment', async (req, res) => {
   const {amount, description} = req.body.product;
   try {
      const paymentIntents = await stripe.paymentIntents.create({
         amount: amount * 100,
         currency: 'usd'
      });
      res.status(200).send({
         clientSecret: paymentIntents.client_secret
      })
   } catch (error) {
      console.log(`Error -> `, error);
      res.status(500).send('');
   }
});

module.exports = route;