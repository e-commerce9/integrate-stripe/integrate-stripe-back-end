const express = require('express');
const server = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const paymentController = require('./controller/payment.controller');

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: true}));
server.use(cors());
server.use('/api', paymentController);
server.listen(process.env.PORT | 3005);
console.log('Server is started ...');
